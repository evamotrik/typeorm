import * as jwt from 'jsonwebtoken'

export const generateTokens = (id: any) =>{
    const payload = {
        id
    }
    const accessToken =  jwt.sign(payload, "SECRET_KEY", {expiresIn: "24h"})
    const refreshToken =  jwt.sign(payload, "SECRET_KEY", {expiresIn: "15d"})
    return {accessToken, refreshToken}
}

export const validateRefreshToken = async (token: string) => {
    try{
        const userData: any = await jwt.verify(token,`${process.env.JWT_REFRESH_SECRET}`);
        return userData
    }catch (e) {
        return null;
    }
}



