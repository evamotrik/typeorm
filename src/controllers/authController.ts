import { User } from "../entity/User"
import { Request, Response } from "express"
import * as bCrypt from "bcrypt"
import * as jwt from 'jsonwebtoken'
import * as config from 'config'
import * as validationSchemaLogin from '../validation/validationLogin'
import * as validationSchemaRegistration from '../validation/validationRegistration'

class authController {
    async registration(req: Request, res: Response) {
        try {
            const { name, surname, email, password } = req.body
            const hashPassword = bCrypt.hashSync(password, 7)
            const user = User.create({ name, surname, email, password: hashPassword })
            await user.save()
            return res.status(201).json(user)
        } catch (err) {
            return res.status(500).json({ message: "Registration error -> " + err })
        }
    }

    async login(req: Request, res: Response) {
        try {
            const { email, password } = req.body;
            const user = await User.findOne({ email })
            if (!user) {
                return res.status(401).json({ message: ` Can't find user ` + email })
            }
            const validPassword = bCrypt.compareSync(password, user.password)
            if (!validPassword) {
                return res.status(402).json({ message: `Incorrect password` })
            }
            const accessToken = jwt.sign({ userId: user.id }, config.get('jwtSecret'), { expiresIn: '1h' });
            const refreshToken = jwt.sign({ userId: user.id }, config.get('jwtSecret'), { expiresIn: '24h' })
            return res.json({
                user: user,
                'accessToken': accessToken,
                'refreshToken': refreshToken
            })
        } catch (e) {
            res.status(400).json({ message: "Authirization error -> " + e })
        }
    }
}

export default new authController
