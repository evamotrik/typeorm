import { Category } from "../entity/Category";
import { Request, Response } from "express";

class categoryController {
    async categoryCreate(req: Request, res: Response){
        try{
            const {name} = req.body
            const category = await Category.findOne({name})
            if(category){
                return res.status(400).json({message:"This category already exist"})
            }
            const obj = Category.create({name})
            await obj.save()
            return res.status(201).json({message: obj})
        }catch (e){
            res.status(400).json({message: "Adding category error -> " + e})
        }
    }

    async deleteCategory (req: Request, res: Response) {
        try{
            const category = await Category.findOne({id: req.params.id})
            if(category){
                Category.delete({id: req.params.id});
                res.send('Category deleted')
            }
            else res.send(`This category does not exist`);
             
        }catch (e){
            res.status(400).json({message: "Deleting category error -> " + e})
        }
    }

    async getById (req: Request, res: Response){
        try{
            const category = await Category.findOne({id: req.params.id})
            res.json({category})
        }catch (e){
            res.status(400).json({message: "Get Category ID error -> " + e})
        } 
    } 

    async getAll (_: Request, res: Response){
        try{
            const categories = await Category.find()
            return res.status(200).json(categories)
        }catch(err){
            res.status(400).json({message: "Get Categories list error -> " + err})
        }
    }

    async updateProduct (req: Request, res: Response){
        try{
            const category = await Category.findOne({id: req.params.id});
            if(!category){
                return res.status(400).json({message:"This category does not exist"})
            }
            const {name} = req.body;
            await Category.update({id: req.params.id}, {name}) 
            res.json({message:"Category changed"})
        }catch (e){
            res.status(400).json({message: "Update category error -> " + e})
        } 
    }
    
}

export default new categoryController
