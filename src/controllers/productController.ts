import { Product } from "../entity/Product";
import { Category } from "../entity/Category";
import { Request, Response } from "express";

class productController {
    async productCreate(req: Request, res: Response){
        try{
            const {name, price, img, category} = req.body
            const product = await Product.findOne({name})
            if(product){
                return res.status(400).json({message:"This product already exist"})
            }
            const obj = Product.create({name, price, img, category})
            await obj.save()
            return res.status(201).json({message: obj})
        }catch (e){
            res.status(400).json({message: "Adding product error -> " + e})
        }
    }

    async deleteProduct (req: Request, res: Response) {
        try{
            const product = await Product.findOne({id: req.params.id})
            if(product){
                Product.delete({id: req.params.id});
                res.send('Product deleted')
            }
            else res.send(`This product does not exist`);
             
        }catch (e){
            res.status(400).json({message: "Deleting product error -> " + e})
        }
    }

    async getById (req: Request, res: Response){
        try{
            const product = await Product.findOne({id: req.params.id})
            res.json({product})
        }catch (e){
            res.status(400).json({message: "Get Product ID error -> " + e})
        } 
    } 

    async getAll (_: Request, res: Response){
        try{
            const products = await Product.find()
            return res.status(200).json(products)
        }catch(err){
            res.status(400).json({message: "Get Products list error -> " + err})
        }
    }

    async updateProduct (req: Request, res: Response){
        try{
            const product = await Product.findOne({id: req.params.id});
            if(!product){
                return res.status(400).json({message:"Нет такого продукта"})
            }
            const {name, price, img} = req.body;
            await Product.update({id: req.params.id}, {name, price, img}) 
            res.json({message:"Product changed"})
        }catch (e){
            res.status(400).json({message: "Update product error -> " + e})
        } 
    }
    
}  

export default new productController
