import * as jwt from 'jsonwebtoken'

const authMiddleware = (req: any, res: any, next: any)=>{
    if(req.method === "OPTIONS"){
        next()
    }
    try{
        const token = req.headers.authorization.split(" ")[1]
        if(!token){
            return res.status(403).json({message: "Пользователь не авторизован"}) 
        }
        const decodedData = jwt.verify(token, "SECRET_KEY")
        req.user = decodedData
        next()
    }catch(e){
        return res.status(403).json({message: "Пользователь не авторизован"})
    }
}

export default authMiddleware