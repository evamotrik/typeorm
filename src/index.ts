import "reflect-metadata";
import { createConnection } from "typeorm";
import { User } from "./entity/User";
import { Product } from "./entity/Product";
import * as express from 'express';
import allRoutes from "./routes/allRoutes"
import * as bodyParser from 'body-parser'
import { Category } from "./entity/Category";
import { SimplePagination } from 'ts-pagination';

const app = express();
const parser = express.json()
const urlencodedParser = bodyParser.urlencoded({ extended: true })
const pagerItemSize = 8; // pager item size per page
const currentIndex = 3; // currentIndex pagination item
const dataTotal = 151; // total data count
const dataSize = 10; // data items per page

const simpleP = new SimplePagination(pagerItemSize, currentIndex, dataTotal, dataSize);

for (let p of simpleP.items) {
    app.use(parser)
    app.use(urlencodedParser)
    app.use('/', allRoutes)
}

createConnection({
    type: 'mysql',
    database: 'TypeOrm',
    username: 'root',
    password: 'root',
    logging: true,
    synchronize: false,
    entities: [User, Product, Category]
})

.then(async(connection) => {
    app.listen('3001', () => {
        console.log("Server Running!")
    })
})

.catch(error => console.log(error))
