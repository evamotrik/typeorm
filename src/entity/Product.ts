import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne} from "typeorm";
import { Category } from "./Category";

@Entity({name: "products"})

export class Product extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @Column({unique: true})
    price: number;

    @Column()
    img: string;

    @ManyToOne (() => Category, category => category.products)
    category: Category;
}