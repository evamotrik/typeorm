import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany} from "typeorm";
import { Product } from "./Product";

@Entity({name: "categories"})

export class Category extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @OneToMany(() => Product, product => product.category)    
    products: Product[];
}