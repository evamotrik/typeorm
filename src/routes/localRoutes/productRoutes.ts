import * as express from 'express'
import productController from '../../controllers/productController'
const router = express.Router()

router.post('/create', productController.productCreate)
router.get('/getById/:id', productController.getById)
router.get('/getAll', productController.getAll)
router.delete('/delete/:id', productController.deleteProduct)
router.post('/update/:id', productController.updateProduct)

export default router