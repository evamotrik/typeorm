import * as express from 'express'
import categoryController from '../../controllers/categoryController'
const router = express.Router()

router.post('/create', categoryController.categoryCreate)
router.get('/getById/:id', categoryController.getById)
router.get('/getAll', categoryController.getAll)
router.delete('/delete/:id', categoryController.deleteCategory)
router.post('/update/:id', categoryController.updateProduct)

export default router