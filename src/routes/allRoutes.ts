import * as express from 'express'
import authRoutes from "./localRoutes/authRoutes"
import productRoutes from "./localRoutes/productRoutes"
import categoryRoutes from "./localRoutes/categoryRoutes"

const router = express.Router()

router.use('/auth', authRoutes);
router.use('/product', productRoutes);
router.use('/category', categoryRoutes);

export default router